package com.company;

import com.company.models.Pays;
import com.company.models.Reduction;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("Hello world");

        double montantHT = 0;
        double montantTVA = 0;
        String codePays = "";
        double tauxTVA = 0;
        double montantTTC = 0;
        double prixUnitaire = 0;
        double quantite = 0;
        double discount = 0;
        double valueDiscount = 0;
        boolean anotherProduct = true;

        ArrayList<Pays> pays = new ArrayList<>();
        Pays france = new Pays("FR", 0.2);
        Pays belgique = new Pays("BE", 0.05);
        Pays roumanie = new Pays("RO", 1);
        Pays ukraine = new Pays("UK", 0.55);
        Pays russie = new Pays("RU", 0.32);
        Pays allemagne = new Pays("DE", 0.15);

        pays.add(france);
        pays.add(belgique);
        pays.add(roumanie);
        pays.add(ukraine);
        pays.add(russie);
        pays.add(allemagne);

        for(Pays onePays : pays){
            System.out.println("Code pays : " + onePays.getIdPays() + " Taux TVA : " + onePays.getTauxTVA());
        }

        Scanner scanner = new Scanner(System.in);

        do {
            System.out.println("Veuillez entrer le prix unitaire du produit :");
            prixUnitaire = Double.parseDouble(scanner.nextLine());
            System.out.println("Veuillez entrer la quantite pour ce produit :");
            quantite = Double.parseDouble(scanner.nextLine());
            montantHT += prixUnitaire*quantite;

            System.out.println("Voulez vous ajouter un autre produit à la facture ? Oui/Non");
            String input = scanner.nextLine();
            if (input.equals("Non")){
                anotherProduct = false;
            }
        } while (anotherProduct);


        System.out.println("Veuillez entrer le code du pays correspondant :");
        codePays = scanner.nextLine();
        for (Pays unPays : pays){
            if (unPays.getIdPays().equals(codePays)){
                tauxTVA = unPays.getTauxTVA();
            }
        }




        ArrayList<Reduction> reductionsPossibles = new ArrayList<>();
        Reduction reduc1 = new Reduction(1000, 0.03);
        Reduction reduc2 = new Reduction(5000, 0.05);
        Reduction reduc3 = new Reduction(7000, 0.07);
        Reduction reduc4 = new Reduction(10000, 0.10);
        Reduction reduc5 = new Reduction(50000, 0.15);

        reductionsPossibles.add(reduc1);
        reductionsPossibles.add(reduc2);
        reductionsPossibles.add(reduc3);
        reductionsPossibles.add(reduc4);
        reductionsPossibles.add(reduc5);

        for(Reduction reduction : reductionsPossibles){
            System.out.println("Palier : " + reduction.getPalier() + "€ Pourcentage réduction : " + reduction.getTaux()*100 + "%");
        }

        if (montantHT>=1000){
            if (montantHT>=5000){
                if(montantHT>=7000){
                    if (montantHT>=10000){
                        if (montantHT>=50000){
                            discount = 0.15;
                        } else {
                            discount = 0.10;
                        }
                    } else {
                        discount = 0.07;
                    }
                } else {
                    discount = 0.05;
                }
            } else {
                discount = 0.03;
            }

        }
        valueDiscount = montantHT*discount;
        montantHT = montantHT - valueDiscount;

        montantTVA = montantHT*tauxTVA;
        montantTTC = montantHT + montantTVA - discount;
        System.out.println("Votre total HT est : " + montantHT);
        System.out.println("Le montant de la réduction appliquée de " + discount*100 + "% est de : " + valueDiscount + "€");
        System.out.println("Le taux de TVA appliqué est de " + tauxTVA*100 + "% pour un total de : " + montantTVA + "€");
        System.out.println("Votre montant total TTC est de : " + montantTTC + "€");

    }
}
