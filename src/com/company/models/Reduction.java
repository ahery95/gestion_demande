package com.company.models;

public class Reduction {

    private int palier;

    private double taux;

    public Reduction(int palier, double taux) {
        this.palier = palier;
        this.taux = taux;
    }

    public int getPalier() {
        return palier;
    }

    public void setPalier(int palier) {
        this.palier = palier;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }
}
