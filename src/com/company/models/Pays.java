package com.company.models;

public class Pays {

    private String idPays;

    private double tauxTVA;

    public Pays(String idPays, double tauxTVA) {
        this.idPays = idPays;
        this.tauxTVA = tauxTVA;
    }

    public String getIdPays() {
        return idPays;
    }

    public void setIdPays(String idPays) {
        this.idPays = idPays;
    }

    public double getTauxTVA() {
        return tauxTVA;
    }

    public void setTauxTVA(double tauxTVA) {
        this.tauxTVA = tauxTVA;
    }
}
